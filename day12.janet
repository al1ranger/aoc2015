#!/usr/bin/env janet

# https://adventofcode.com/2015/day/12

# For parsing JSON, the spork module will be used
# See https://janet-lang.org/api/spork/index.html
(import spork/json)

# Collect all numbers in the input, ignoring the object when the
# optional stringValueToIgnore is present
(defn collectNumbers [input numbers &opt stringValueToIgnore]
  (case (type input)
    # Process table only if value is not found
    :table (if (not (has-value? input stringValueToIgnore)) 
      (eachp [k v] input (collectNumbers v numbers stringValueToIgnore))
    )

    # Strings will anyway be ignored when looking for numbers
    :array (each v input (collectNumbers v numbers stringValueToIgnore))
    
    # Add to list
    :number (array/push numbers input)
  )
)

# Total of all numbers in the input JSON
(defn part1 [inputJSON]
  (def numbers @[])
  (collectNumbers inputJSON numbers)
  (print "Part 1: " (sum numbers))
)

# Total of all numbers in the input JSON, excluding objects that have 
# the value "red"
(defn part2 [inputJSON]
  (def numbers @[])
  (collectNumbers inputJSON numbers "red")
  (print "Part 2: " (sum numbers))
)

(defn main [& args]
  # Read data from file and parse the JSON into a Janet object
  (def inputJSON (json/decode (string/trimr (slurp (args 1)) "\n")))
  
  # Solve Part 1
  (part1 inputJSON)

  # Solve Part 2
  (part2 inputJSON)
)
