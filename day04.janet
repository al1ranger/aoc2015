#!/usr/bin/env janet

# https://adventofcode.com/2015/day/4

(import codec) # For calculating hashes

# Find a number such that the MD5 hash of the string formed by concatenating 
# secret and number starts with hashStart
(defn findNumber [secret hashStart]
  (var counter 0)
  (def lengthToCheck (length hashStart))

  # Initial hash value so that the check in while does not give error
  (var md5Hash (string/repeat "-" lengthToCheck))

  # Check for each value of counter till a match is found
  (while (not= (string/slice md5Hash 0 lengthToCheck) hashStart)
    (++ counter)
    (set md5Hash (codec/md5 (string secret counter)))
  )

  counter # Return the value
)

(defn main [& args]
  # Read secret from file
  (def secret (slurp (args 1)))

  # Number that gives hash with five leading zeros
  (print "Part 1: " (findNumber secret "00000"))

  # Number that gives hash with six leading zeros
  (print "Part 1: " (findNumber secret "000000"))
)
