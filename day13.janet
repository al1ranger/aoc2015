#!/usr/bin/env janet

# https://adventofcode.com/2015/day/13

# See https://janet-lang.org/docs/peg.html for an introduction to 
# Parsing Expression Grammars

# Build a matrix of happiness values based on the input rules
(defn buildHappinessMatrix [rules]
  (def matcher (peg/compile ~(* (<- :w+) " would " (<- :w+) " " (<- :d+) " happiness units by sitting next to " (<- :w+) ".")))
  (def happinessMatrix @{})

  (var matches @[])
  (var person1 nil)
  (var person2 nil)
  (var happiness 0)

  (each rule rules
    # Extract data from the sentence
    # Calculate happiness and store in map
    (set matches (peg/match matcher rule))
    (set person1 (symbol (matches 0)))
    (set person2 (symbol (matches 3)))
    (set happiness (* 
      (if (= (matches 1) "lose") -1 1) 
      (int/to-number (int/s64 (matches 2))))
    )
    
    (put-in happinessMatrix [person1 person2] happiness)
  )

  happinessMatrix
)

# Generate all possible seating arrangements of remaining considering 
# those that are aready seated
(defn getSeatingArrangements [remaining seated arrangements]
  (if (= (length remaining) 2)
    # Direct processing to reduce recursion depth
    (for i 0 2
      (for j 0 2
        (if (not= i j)
          (array/push arrangements @[(splice seated) (remaining i) (remaining j)])
        )
      )
    )
    (do
      (var toBeSeated @[])
      (var alreadySeated @[])

      (for i 0 (length remaining)
        # Remove current person from list of those remaining
        (set toBeSeated (array/remove (array/slice remaining) i))
        
        # Add current person to the list of those seated
        (set alreadySeated (array/concat @[] seated (remaining i)))

        # Get seating arrangements
        (getSeatingArrangements toBeSeated alreadySeated arrangements)
      )
    )
  )
)

# Calculates the happiness for a given seating arrangement
(defn calculateHappiness [happinessMatrix arrangement]
  (var happiness 0)
  (def items (length arrangement))
  
  # For each person, add the happiness score with person on the left and right
  (for i 0 items
    (+= happiness 
        (get-in happinessMatrix [(arrangement i) (arrangement (mod (- i 1) items))])
        (get-in happinessMatrix [(arrangement i) (arrangement (mod (+ i 1) items))])
    )
  )
  
  happiness
)

# Maximum happiness using the rules provided in puzzle input
(defn part1 [happinessMatrix]
  (def arrangements @[])
  (getSeatingArrangements (keys happinessMatrix) @[] arrangements)
  
  (print "Part 1: " (max (splice 
      (map (fn [x] (calculateHappiness happinessMatrix x)) arrangements))
    )
  )
)

# Maximum happiness after adding the player to the group
(defn part2 [happinessMatrix]
  (def newHappinessMatrix (table/clone happinessMatrix))
  
  # Add the rules for the player
  (each person (keys happinessMatrix)
    (put-in newHappinessMatrix [:Player person] 0)
    (put-in newHappinessMatrix [person :Player] 0)
  )  

  (def arrangements @[])
  (getSeatingArrangements (keys newHappinessMatrix) @[] arrangements)
  
  (print "Part 2: " (max (splice 
      (map (fn [x] (calculateHappiness newHappinessMatrix x)) arrangements))
    )
  )
)

(defn main [& args]
  # Read input file and build rules
  (def happinessMatrix (buildHappinessMatrix 
      (string/split "\n" (string/trimr (slurp (args 1)) "\n"))
    )
  )

  # Solve Part 1
  (part1 happinessMatrix)

  # Solve Part 2
  (part2 happinessMatrix)
)
