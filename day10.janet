#!/usr/bin/env janet

# https://adventofcode.com/2015/day/10

# Here input file contains only one line of input

# Performs Look and Say on the input sequence and stores the
# result in the same parameter
(defn lookAndSay [sequence]
  (def nextSequence @[])
  (var number (sequence 0))
  (var occurrences 1)

  (for i 1 (length sequence)
    (if (not= number (sequence i))
      # A new number, so store the details of the previous number
      (do
        (array/push nextSequence occurrences number)
        (set number (sequence i))
        (set occurrences 1)
      )
      (++ occurrences)
    )
  )

  # Store details of the last number
  (array/push nextSequence occurrences number)

  # Store result
  (array/clear sequence)
  (flatten-into sequence nextSequence)
)

# Length of result after Look and Say is performed 40 times
(defn part1 [startSequence]
  (repeat 40
    (lookAndSay startSequence)
  )

  (print "Part 1: " (length startSequence))
)

# Length of result after Look and Say is performed 50 times
(defn part2 [startSequence]
  # As the process was performed 40 times for part 1, only
  # the delta needs to be performed here
  (repeat 10
    (lookAndSay startSequence)
  )

  (print "Part 2: " (length startSequence))
)

(defn main [& args]
  # Read input and store as a sequence of numbers
  (var startSequence (map 
    (fn [x] (int/to-number (int/s64 (string/from-bytes x)))) 
    (string/trimr (slurp (args 1)) "\n"))
  )

  # Solve Part 1
  (part1 startSequence)

  # Solve Part 2 using output of part 1 to improve performance
  (part2 startSequence)
)
