(declare-project 
  :name "aoc2015"
  :description "Solutions for 2015 Advent of Code"

  :dependencies ["https://github.com/joy-framework/codec" "spork"]
)