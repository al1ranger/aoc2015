#!/usr/bin/env janet

# https://adventofcode.com/2015/day/14

# See https://janet-lang.org/docs/peg.html for an introduction to 
# Parsing Expression Grammars

# Parse each sentence and extract the required details
(defn readRacerDetails [details]
  (def matcher (peg/compile  ~(* (<- :w+) " can fly " (<- :d+) 
    " km/s for " (<- :d+) " seconds, but then must rest for " 
    (<- :d+) " seconds.")
    )
  )
  (var racers @{})
  (var matches @[])

  (each detail details
    (set matches (peg/match matcher detail))
    
    # Update list of racers
    (put racers (keyword (matches 0)) (map 
        (fn [x] (int/to-number (int/u64 x)))
        @[(matches 1) (matches 2) (matches 3)]
      )
    )
  )

  racers
)

# racerDetails is an array containing 
# [<speed> <seconds flown> <seconds rested>]
# Returns the distance travelled
(defn calculateDistanceTravelled [racerDetails seconds]
  (var distance 0)
  (var timeRemaining seconds)
  (var timetoReduce 0)

  (while (>= timeRemaining 0)
    # Update distance and reduce time remaining
    (set timetoReduce (min (racerDetails 1) timeRemaining))
    (+= distance (* timetoReduce (racerDetails 0)) )
    (-= timeRemaining timetoReduce)

    # Time up
    (if (<= timeRemaining 0) (break))

    # Reduce time remaining by idle time
    (set timetoReduce (min (racerDetails 2) timeRemaining))
    (-= timeRemaining timetoReduce)
  )

  distance
)

# Distance travelled by winner after 2503 seconds
(defn part1 [racers]
  (print "Part 1: " (max (splice 
      (map (fn [x] (calculateDistanceTravelled x 2503)) racers)
    ))
  )
) 

# Each value in table racerScores is an array of the form 
# @[<speed> <time flown> <time rested> <flight time remaining> 
# <rest time remainng> <distance travelled> <score>]}
# Updates the <distance travelled> and <score> for each racer
(defn updateScore [racerScores]
  (var maxDistance 0)

  (each racer racerScores
    (if (> (racer 3) 0) 
      # Update distance travelled if flying
      # Decrease remaining time for flight
      (do
        (+= (racer 5) (racer 0))
        (-= (racer 3) 1)
      )

      # Decrease remaining time of rest
      (if (> (racer 4) 0)
        (-= (racer 4) 1)
      )
    )
    
    # Fully rested, so ready to fly again
    (if (= (racer 4) 0)
      (do
        (set (racer 3) (racer 1))
        (set (racer 4) (racer 2))
      )
    )
  )

  # Determine the maximum distance
  (set maxDistance (max (splice (map (fn [x] (x 5)) racerScores))))

  # Update scores
  (each racer racerScores
    (if (= maxDistance (racer 5))
      (+= (racer 6) 1)
    )
  )
)

# Points obtained by winner after 2503 seconds
(defn part2 [racers]
  (var racerScores @{})
  
  # Build extended array of details for each racer
  (eachp [racer details] racers
    (put racerScores racer 
      @[(splice details) (details 1) (details 2) 0 0]
    )
  )

  # Calculate the scores
  (for i 0 2503
    (updateScore racerScores)
  )

  (print "Part 2: " (max (splice (map (fn [x] (x 6)) racerScores))))
)

(defn main [& args]
  # Read input and determine racer details
  (def racers (readRacerDetails 
      (string/split "\n" (string/trimr (slurp (args 1)) "\n"))
    )
  )

  # Solve Part 1
  (part1 racers)

  # Solve Part 2
  (part2 racers)
)
