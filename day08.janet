#!/usr/bin/env janet

# https://adventofcode.com/2015/day/8
# Based on https://www.reddit.com/r/adventofcode/comments/3vw32y/day_8_solutions/

# Janet's eval-string function can be used to get the length of the string after 
# the escape sequences have been evaluated.
# For example, if the string is "\x27" (6 characters) calling eval-string on
# (length "\x27") returns 1.
(defn part1 [lines]
  (print "Part 1: " (apply + (map 
    (fn [x] (- (length x) (eval-string (string "(length " x ")")))) lines))
  )
)

# For each line of input, two characters will get added for the two double quotes 
# at the start and end. One extra character will be added for each double quote and
# backslash. So, the required difference for each line is 
# 2 + <number of backslashes> + <number of quotes>.
(defn part2 [lines]
  (print "Part 2: " (apply + (map 
    (fn [x] (+ 2 (length (string/find-all "\\" x)) (length (string/find-all "\"" x)))) lines))
  )
)

(defn main [& args]
  # Read input
  (def lines (string/split "\n" (string/trimr (slurp (args 1)) "\n")))

  # Solve Part 1
  (part1 lines)
  
  # Solve Part 2
  (part2 lines)
)
