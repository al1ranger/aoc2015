#!/usr/bin/env janet

# https://adventofcode.com/2015/day/7
# See https://janet-lang.org/docs/peg.html for an introduction to 
# Parsing Expression Grammars

# Gets the value of input from results. 
# If input is a number, returns it instead.
(defn getValue [results input]
  (if (nil? (peg/find :a input)) # Check if number
    (int/s64 input) # Convert to number
    (get results input) # Get from results
  )
)

# Processes the instructions in circuits. 
# Returns a table containing values of the individual gates.
(defn processInstructions [circuits]
  (def results @{})
  (def circuitsToProcess (table/clone circuits))

  (while (not (empty? circuitsToProcess))
    (eachk gate circuitsToProcess
      (def inputs (get circuitsToProcess gate))
      (case (length inputs)
        1 (do # Either number or another gate
          (def input (getValue results (inputs 0)))
          (if input
            (do
              (put results gate input) # Store gate value
              (put circuitsToProcess gate nil) # Remove from list
            )
          )
        )

        2 (do # Unary operator with gate or a number
          (def input (getValue results (inputs 1)))
          (if input 
            (do
              (case (inputs 0)
                "NOT" (put results gate (bnot (int/to-number input)))
              )
              (put circuitsToProcess gate nil)
            )
          )
        )

        3 (do # Binary operators with two operands
          (def lhs (getValue results (inputs 0)))
          (def rhs (getValue results (inputs 2)))
          (if (and lhs rhs)
            (do
              (case (inputs 1)
                "AND" (put results gate (band lhs rhs))
                "OR" (put results gate (bor lhs rhs))
                "LSHIFT" (put results gate (blshift lhs rhs))
                "RSHIFT" (put results gate (brshift lhs rhs))
              )
              (put circuitsToProcess gate nil)
            )
          )
        )
      )
    )
  )

  results
)

(defn part1 [circuits]
  (def a (get (processInstructions circuits) "a"))
  (print "Part 1: " (if (< a 0) (+ 65536 a) a))
)

(defn part2 [circuits]
  (def newCircuits (table/clone circuits))
  
  # Replace input to b with output of a
  (put newCircuits "b" @[(string (get (processInstructions circuits) "a"))])

  (def a (get (processInstructions newCircuits) "a"))
  (print "Part 2: " (if (< a 0) (+ 65536 a) a))
)

(defn main [& args]
  (def circuits @{})
  
  # Read circuit details from file
  (def lines (string/split "\n" (string/trimr (slurp (args 1)) "\n")))
  (each line lines
    (def parts (string/split " -> " line))
    (put circuits (parts 1) (string/split " " (parts 0)))
  )

  # Solve Part 1
  (part1 circuits)

  # Solve Part 2
  (part2 circuits)
)
