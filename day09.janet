#!/usr/bin/env janet

# https://adventofcode.com/2015/day/9

(defn readDistanceDetails [lines]
  # In the file, distances are listed from the first city to the other cities, 
  # then from the second city to the others, except the first and so on.
  # That means, if there are n cities, the number of lines in the file is
  # n * (n - 1) / 2, i.e. n^2 - n - 2 * lines = 0, which is a quadratic equation
  # with a = 1, b = -1 and c = -2 * lines. Solving this equation and taking the
  # higher value will give the number of cities. 
  (def bSquaredMinus4ac (math/sqrt (+ 1 (* 8 (length lines)))))
  (def cities (/ (max (+ 1 bSquaredMinus4ac) (- 1 bSquaredMinus4ac)) 2))

  (def distances (array/new cities))
  (def cityNames @[])

  # Initialize all distances to the maximum possible
  (for i 0 cities
    (put distances i (array/new-filled cities math/int-max))
  )

  # Extract the details from each line and set in the two dimensional array.
  # As distances are symmetrical, the same number will appear at two places.
  (def lineFormat (peg/compile ~(* (<- :a+) " to " (<- :a+) " = " (<- :d+))))
  (each line lines
    (var data (peg/match lineFormat line))

    # Add any new cities to the list of cities
    (if (= (index-of (data 0) cityNames -1) -1)
      (array/push cityNames (data 0))
    )
    
    (if (= (index-of (data 1) cityNames -1) -1)
      (array/push cityNames (data 1))
    )
    
    # Update distances
    (set ((distances (index-of (data 0) cityNames -1)) (index-of (data 1) cityNames -1)) 
      (int/to-number (int/s64 (data 2))))
    (set ((distances (index-of (data 1) cityNames -1)) (index-of (data 0) cityNames -1)) 
      (int/to-number (int/s64 (data 2))))
  )

  # Return the result as a table
  {:distances distances :cities cityNames}
)

# Gets all routes give a list of cities already visited and cities to be visited.
# The routes are added to the array allRoutes.
(defn getAllRoutes [visited toVisit allRoutes]
  # For two entries, generate manually to reduce recursion depth
  (if (= (length toVisit) 2)
    (for i 0 2
      (for j 0 2
        (if (not= i j)
          (array/push allRoutes (flatten @[visited (toVisit i) (toVisit j)]))
        )
      )
    )
    (do
      # Repeatedly pick one city to visit next and get all possible routes 
      # for the remaining
      (for i 0 (length toVisit)
        (getAllRoutes 
          (flatten @[visited (toVisit i)]) # Select a city and mark it as visited
          (map # Get the list of cities yet to be visited
            (fn [x] (get toVisit x)) 
            (filter (fn [x] (not= x i)) (range 0 (length toVisit)))
          )
          allRoutes # Accumulator
        ) 
      )
    )
  )
)

# Calulates the total distance for a route
(defn calculateRouteDistance [route distances]
  (sum # Total the indiviual distances
    (map # Get the list of distances travelled
      (fn [x] (get (distances (route x)) (route (inc x)))) 
      (range 0 (dec (length route)))
    )
  )
)

# Calculates the distances for all the routes
(defn calculateAllDistances [distanceDetails]
  (def allRoutes @[])

  # Initially, no city is visited. So, all cities are in the to be visited list.
  (getAllRoutes 
    @[] (range 0 (length (get distanceDetails :cities))) allRoutes
  )

  # Calculate the distances for the routes
  (map 
    (fn [x] (calculateRouteDistance x (get distanceDetails :distances))) 
    allRoutes
  )
)

# The shortest route
(defn part1 [distanceDetails]
  (print "Part 1: " (min-of (calculateAllDistances distanceDetails)))
)

# The longest route
(defn part2 [distanceDetails]
  (print "Part 2: " (max-of (calculateAllDistances distanceDetails)))
)

(defn main [& args]
  # Read input
  (def distanceDetails (readDistanceDetails 
    (string/split "\n" (string/trimr (slurp (args 1)) "\n")))
  )
  
  # Solve Part 1
  (part1 distanceDetails)
  
  # Solve Part 2
  (part2 distanceDetails)
)
