#!/usr/bin/env janet

# https://adventofcode.com/2015/day/1

# Only the final floor is required
(defn part1 [instructions]
  # ( => go up and ) => go down
  (print "Part 1: " (- (length (string/find-all "(" instructions)) (length (string/find-all ")" instructions))))
)

# Calculate the position when the basement is visited first
(defn part2 [instructions]
  (var floor 0)
  (var position 0)
  
  # Process each character of the input as ( => go up and ) => go down
  # Value of floor will be -1 for basement
  (loop [instruction :iterate (string/slice instructions position (inc position)) :until (= floor -1)]
    (cond 
      (= instruction "(") (++ floor)
      (= instruction ")") (-- floor)
    )
    
    (++ position)
  )

  (print "Part 2: " position)
)

(defn main [& args]
  # Get data from the file
  (def fileHandle (file/open (args 1) :r))
  (def instructions (file/read fileHandle :all))
  (file/close fileHandle)

  # Solve part 1
  (part1 instructions)
  
  # Solve part 2
  (part2 instructions)
)
