#!/usr/bin/env janet

# https://adventofcode.com/2015/day/6
# See https://janet-lang.org/docs/peg.html for an introduction to 
# Parsing Expression Grammars

# PEG to match the instruction and extract the start and end points
(def instructionPattern (peg/compile ~{ 
      :on (* (capture "turn on ") (capture (some :d)) "," (capture (some :d)) 
      " through " (capture (some :d)) "," (capture (some :d)))
      :off (* (capture "turn off ") (capture (some :d)) "," (capture (some :d)) 
      " through " (capture (some :d)) "," (capture (some :d)))
      :toggle (* (capture "toggle ") (capture (some :d)) "," (capture (some :d)) 
      " through " (capture (some :d)) "," (capture (some :d)))
      :main (+ :on :off :toggle)
    }
  ) 
)

(defn decodeInstruction [instruction]
  # Use the PEG defined earlier to extract the operation and points
  (def result (peg/match instructionPattern instruction))

  (if (nil? result)
    @[]
    # Convert instruction to a keyword (:turnon, :turnoff and :toggle)
    # Convert the co-ordinates to numbers
    @[(keyword (string/replace-all " " "" (result 0))) (int/s64 (result 1)) 
      (int/s64 (result 2)) (int/s64 (result 3)) (int/s64 (result 4))]
  )
)

(defn part1 [instructions]
  (def lights @{})

  (each instruction instructions
    (def result (decodeInstruction instruction))

    (for x (result 1) (inc (result 3))
      (for y (result 2) (inc (result 4))
        (var key (string x "_" y)) 
        (var switch (get lights key)) # value is nil if absent

        # Update switch based on the instruction
        (case (result 0)
          :turnon (set switch true)
          :turnoff (set switch false)
          :toggle (toggle switch)
        )

        # Store the updated value in the table
        # If switch is off, delete it from the table
        (put lights key (if (false? switch) nil switch))
      )
    )
  )

  (print "Part 1: " (length lights))
)

(defn part2 [instructions]
  (def lights @{})

  (each instruction instructions
    (def result (decodeInstruction instruction))

    (for x (result 1) (inc (result 3))
      (for y (result 2) (inc (result 4))
        (var key (string x "_" y))
        (var brightness (get lights key 0)) # Brightness is 0 if absent

        # Update brightness based on the instruction
        # Brightness cannot exceed 1000 or decrease below 0
        (case (result 0)
          :turnon (set brightness (min (inc brightness) 1000))
          :turnoff (set brightness (max (dec brightness) 0))
          :toggle (set brightness (min (+ 2 brightness) 1000))
        )
        
        # Set updated brightness
        # If brightness is 0, delete it from the table
        (put lights key (if (> brightness 0) brightness nil))
      )
    )
  )

  (print "Part 2: " (apply + (values lights)))
)

(defn main[& args]
  # Read instructions from file
  (def instructions (string/split "\n" (string/trimr (slurp (args 1)) "\n")))

  # Solve Part 1
  (part1 instructions)

  # Solve Part 2
  (part2 instructions)
)
