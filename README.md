# Advent of Code 2015 Solutions

## About
This repository contains the solutions for the puzzles in the [2015 Advent of Code calendar](https://adventofcode.com/2015).

The solutions are in the [Janet](https://janet-lang.org) programming language.

There is one file for each day. The files are named `day01.janet`, `day02.janet` and so on.

For all the programs, the file containing the puzzle input needs to be specified on the command line.

[//]: # ( While the event is running, the solution will be committed to this repository after the deadline for that puzzle has expired or a working solution is found, whichever is later. )

The code shared here may be slightly different than what was used to actually solve the puzzle, as it may have been refactored or updated with  comments.

## Compiling and Running
1. [Install](https://janet-lang.org/docs/index.html) Janet
2. Navigate to the root directory in a terminal and execute `jpm deps` (run as super user if it gives a 'Permission denied' error) to install the dependencies
3. To run a file, execute the following command (see `janet -h` for all available options)   
`janet dayxx.janet dayxx.txt`  
where `dayxx.janet` is the Janet source file and `dayxx.txt` is the puzzle input
