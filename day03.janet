#!/usr/bin/env janet

# https://adventofcode.com/2015/day/3

# Return houses visited by a given number of Santas, with each Santa following directions in sequence
(defn housesVisited [directions santas]
  (def x (array/new-filled santas 0)) # (Santa1 Santa2 ...)
  (def y (array/new-filled santas 0)) # (Santa1 Santa2 ...)
  (var house "")
  (var santaIndex 0)

  (def houses @{})

  # The starting house gets one present from each Santa
  (put houses "0_0" santas)

  # Update x and y based on direction
  # Determine which Santa will move based on the index of the current direction
  (for i 0 (length directions)
    (set santaIndex (% i santas))

    (case (string/from-bytes (directions i))
      "^" (++ (y santaIndex))
      "v" (-- (y santaIndex))
      ">" (++ (x santaIndex))
      "<" (-- (x santaIndex))
    )

    # Build key for house from x and y co-ordinates
    (set house (string (x santaIndex) "_" (y santaIndex)))
    
    # Increment value by 1, or store 1 if not found
    (put houses house (inc (get houses house 0)))
  )

  # Return the number of houses visited
  (length houses)
)

(defn main [& args]
  (var directions "")

  # Read directions from file
  (with [fileHandle (file/open (args 1) :r)]
    (set directions (string/trimr (file/read fileHandle :all) "\n"))
  )

  # Both parts are special cases of the general case of n Santas following instructions in sequence

  # Solve part 1
  (print "Part 1: " (housesVisited directions 1))

  # Solve part 2
  (print "Part 2: " (housesVisited directions 2))
)
