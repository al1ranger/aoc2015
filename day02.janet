#!/usr/bin/env janet

# https://adventofcode.com/2015/day/2

# Calculate the wrapping paper required
(defn part1 [dimensions]
  (var sqFeetOfPaper 0)

  (each dimension dimensions
    # Paper required is surface area + area of the smallest box
    # As each dimension is sorted, the first two elements of each dimension are also the smallest
    (+= sqFeetOfPaper 
      (+ (* 3 (dimension 0) (dimension 1)) 
         (* 2 (dimension 1) (dimension 2)) 
         (* 2 (dimension 2) (dimension 0))
      )
    )
  )

  (print "Part 1: " sqFeetOfPaper)
)

# Calculate the ribbon required
(defn part2 [dimensions]
  (var feetOfRibbon 0)

  (each dimension dimensions
    (+= feetOfRibbon
      (+ (* 2 (dimension 0)) (* 2 (dimension 1)) # The smallest perimeter
         (apply * dimension) # Volume
      )
    )
  )

  (print "Part 2: " feetOfRibbon)
)

(defn main [& args]
  (def dimensions @[])
  
  # Get data from the file and build array of dimensions
  (with [fileHandle (file/open (args 1) :r)]
    (loop [line :iterate (file/read fileHandle :line)]
      (array/insert dimensions -1 # Append to array
        (sort # Sort in ascending order
          (map int/s64 # Convert to integer
            (string/split "x" # Dimensions are separated by x
              (string/trimr line "\n") # Remove trailing newline
            ) 
          )
        )
      )
    )
  )

  # Solve part 1
  (part1 dimensions)
  
  # Solve part 2
  (part2 dimensions)
)
