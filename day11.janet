#!/usr/bin/env janet

# https://adventofcode.com/2015/day/11

# See https://janet-lang.org/docs/peg.html for an introduction to 
# Parsing Expression Grammars

# Here input file contains only one line of input

# The three rules built as PEGs
(var rule1 nil)
(var rule2 nil)
(var rule3 nil)

(defn buildRules []
  # At least one of "abc", "def", ... "xyz"
  (if (nil? rule1)
    (do
      (def rule1Matches @[])
      (for i 0 24
        (array/push rule1Matches 
          (string/from-bytes (+ 97 i) (+ i 98) (+ i 99))
        )
      )
      (set rule1 (peg/compile ~(+ ,(splice rule1Matches))))
    )
  )

  # i, l or o
  (if (nil? rule2)
    (do
      (set rule2 (peg/compile ~(+ "i" "l" "o")))
    )
  )

  # At least 2 of "aa", "bb", ..., "zz"
  (if (nil? rule3)
    (do
      (def rule3Matches @[])
      (for i 0 26
        (array/push rule3Matches 
          (string/repeat (string/from-bytes (+ 97 i)) 2)
        )
      )
      (set rule3 (peg/compile ~(<- (+ ,(splice rule3Matches)))))
    )
  )
)

(defn generateNewPassword [oldPassword]
  # Break string into letters (integer values)
  (def letters (flatten (string/bytes oldPassword)))
  
  # Increment the right most letter
  # If overflow occurs, pass it on to the letter on the left
  # In the ASCII character set 'a' is 97 and 'z' is 122
  
  (var carry 0)
  (def rightMost (dec (length letters)))

  (loop [i :down-to (rightMost 0)] 
    (if (= i rightMost)
      (++ (letters i))
      (if (> carry 0)
        (do
          (+= (letters i) carry)
          (set carry 0)
        )
      )
    )
    
    (if (> (letters i) 122)
      (do
        (++ carry)
        (set (letters i) 97)
      )
    )
    
    (if (= carry 0)
      (break)
    )
  )

  # Build the new string
  (string/join (map string/from-bytes letters))
)

# Check the conditions for rule #3
(defn rule3Check [password]
  (def matches (peg/find-all rule3 password))

  (and
    (>= (length matches) 2) # At least two matches
    (> (- (max (splice matches)) (min (splice matches))) 1) # No overlapping
    (>= # Should not repeat
      (length (distinct 
        (flatten (map (fn [x] (peg/match rule3 password x)) matches))
        )
      ) 
      2
    )
  )
)

(defn isValid [password]
  (and 
    (>= (length (peg/find-all rule1 password)) 1)
    (= (length (peg/find-all rule2 password)) 0)
    (rule3Check password)
  )
)

(defn getNextPassword [oldPassword part]
  (var newPassword (generateNewPassword oldPassword))
  
  (while (not (isValid newPassword))
    (set newPassword (generateNewPassword newPassword))
  )
  
  (printf "Part %d: %s" part newPassword)
  
  # Return the final password
  newPassword
)

(defn main [& args]
  (var password (string/trimr (slurp (args 1)) "\n"))
  
  (buildRules)

  # Solve Part 1
  (set password (getNextPassword password 1))

  # Solve Part 2
  (set password (getNextPassword password 2))
)
