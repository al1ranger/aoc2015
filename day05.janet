#!/usr/bin/env janet

# https://adventofcode.com/2015/day/5
# See https://janet-lang.org/docs/peg.html for an introduction to 
# Parsing Expression Grammars

(defn part1 [words]
  # Check whether the input satisfies the conditions
  (defn isNiceString [input]
    (and 
      (empty? 
        (peg/find-all 
          ~(+ "ab" "cd" "pq" "xy") input
        )
      ) # ab, cd, xy and pq must not be present
      (>= 
        (length 
          (peg/find-all 
            ~(set "aeiou") input
          )
        ) 
        3
      ) # At least 3 vowels
      (not 
        (empty? 
          (peg/find-all 
            ~(* (capture :a :l)(backmatch :l)) input
          )
        )
      ) # Letter appearing twice
    )
  )

  (print "Part 1: " (count true? (map isNiceString words)))
)

(defn part2 [words]
  # Check whether the input satisfies the conditions
  (defn isNiceString [input]
    (and 
      (not 
        (empty? 
          (peg/find-all 
            ~(* (capture (repeat 2 :a) :l1) (to (backmatch :l1))) input
          )
        )
      ) # Two letters that appear twice without overlapping
      (not 
        (empty? 
          (peg/find-all 
            ~(* (capture :a :l1) 1 (backmatch :l1)) input
          )
        )
      ) # Letter repeating after exactly one letter
    )
  )

  (print "Part 2: " (count true? (map isNiceString words)))
)

(defn main [& args]
  (def words (string/split "\n" (string/trimr (slurp (args 1)) "\n")))

  # Solve Part 1
  (part1 words)

  # Solve Part 2
  (part2 words)
)
